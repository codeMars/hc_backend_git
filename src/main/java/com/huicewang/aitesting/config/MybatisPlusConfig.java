package com.huicewang.aitesting.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: aitesting
 * @author: caosenquan
 * @create: 2021-08-26 21:57
 **/

//@Configuration
//public class MybatisPlusConfig {
//    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        return new PaginationInterceptor();
//    }
//
//}