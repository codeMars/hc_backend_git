package com.huicewang.aitesting.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootConfiguration
@EnableScheduling
public class SpringTaskConfig {
}
