package com.huicewang.aitesting.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import org.springframework.context.annotation.Configuration;
//
///**
// * 添加MyBatis-Plus的Java配置，使用@MapperScan注解配置好需要扫描的Mapper接口路径，
// * MyBatis-Plus自带分页功能，需要配置好分页插件PaginationInterceptor
// */
@SpringBootConfiguration
@MapperScan("com.huicewang.aitesting.mapper")
public class MyBatisConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        return paginationInterceptor;
    }
}