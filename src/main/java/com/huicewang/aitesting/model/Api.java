package com.huicewang.aitesting.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author huice
 * @since 2021-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("api")
@ApiModel(value="Api对象", description="")
public class Api implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "接口id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "接口名")
    private String name;

    @ApiModelProperty(value = "接口版本")
    private String version;

    @ApiModelProperty(value = "接口状态（是否废弃）")
    private Integer status;

    @ApiModelProperty(value = "接口请求类型")
    private String method;

    @ApiModelProperty(value = "接口协议（http或https）")
    private String protocol;

    @ApiModelProperty(value = "接口路径")
    private String path;

    @ApiModelProperty(value = "公共参数")
    @TableField("commonParams")
    private String commonParams;

    @ApiModelProperty(value = "接口参数")
    private String params;

    @ApiModelProperty(value = "请求body")
    private String body;

    @ApiModelProperty(value = "请求头")
    private String headers;

    @ApiModelProperty(value = "请求cookie")
    private String cookies;

    @ApiModelProperty(value = "接口描述")
    private String description;

    @ApiModelProperty(value = "返回结果")
    private String response;

    @ApiModelProperty(value = "添加人")
    private String author;

    @ApiModelProperty(value = "创建时间")
    @TableField("createTime")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("updateTime")
    private Date updateTime;

    @ApiModelProperty(value = "分组id")
    @TableField("apiGroupId")
    private Integer apiGroupId;


    @TableField("projectId")
    private Integer projectId;

//    private int pageIndex;//分页的索引
//    private int pageSize;//分页的大小
//    private int startSize;//分页起始行

    @Override
    public String toString() {
        return "Api{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", status=" + status +
                ", method='" + method + '\'' +
                ", protocol='" + protocol + '\'' +
                ", path='" + path + '\'' +
                ", commonParams='" + commonParams + '\'' +
                ", params='" + params + '\'' +
                ", body='" + body + '\'' +
                ", headers='" + headers + '\'' +
                ", cookies='" + cookies + '\'' +
                ", description='" + description + '\'' +
                ", response='" + response + '\'' +
                ", author='" + author + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", apiGroupId=" + apiGroupId +
                '}';
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCommonParams() {
        return commonParams;
    }

    public void setCommonParams(String commonParams) {
        this.commonParams = commonParams;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getApiGroupId() {
        return apiGroupId;
    }

    public void setApiGroupId(Integer apiGroupId) {
        this.apiGroupId = apiGroupId;
    }

//    public int getPageIndex() {
//        return pageIndex;
//    }
//
//    public void setPageIndex(int pageIndex) {
//        this.pageIndex = pageIndex;
//    }
//
//    public int getPageSize() {
//        return pageSize;
//    }
//
//    public void setPageSize(int pageSize) {
//        this.pageSize = pageSize;
//    }
//
//    public int getStartSize() {
//        return startSize;
//    }
//
//    public void setStartSize(int startSize) {
//        this.startSize = startSize;
//    }
//
//    public void setPage(int pageIndex, int pageSize){
//        this.pageIndex = pageIndex;
//        this.pageSize = pageSize;
//        this.startSize = (pageIndex - 1) * pageSize;
//    }
}
