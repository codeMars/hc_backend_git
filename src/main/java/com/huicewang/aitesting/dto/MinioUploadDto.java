package com.huicewang.aitesting.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 文件上传返回结果
 * Created by huice on 2021/7/22.
 */
@Getter
@Setter
public class MinioUploadDto {
    private String url;
    private String name;
}
