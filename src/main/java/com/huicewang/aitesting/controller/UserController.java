package com.huicewang.aitesting.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huicewang.aitesting.common.CommonPage;
import com.huicewang.aitesting.common.CommonResult;
import com.huicewang.aitesting.model.User;
import com.huicewang.aitesting.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  用户管理Controller
 * </p>
 *
 * @author huice
 * @since 2021-07-14
 */
@Api(tags = "UserController", description = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {
    // 定义一个日志对象
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    // http://localhost:9000/user/12

//    @RequestMapping(value = "/testing/{id}", method = RequestMethod.GET)
//    public void get(@PathVariable Integer id){
//        userService.get(id);
//    }

    @ApiOperation("获取指定id的用户信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CommonResult<User> getById(@PathVariable Long id){
        return CommonResult.success(userService.getById(id));
    }

    @ApiOperation("获取所有用户列表")
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public CommonResult<List<User>> getList(){
        return CommonResult.success(userService.list());
    }

    @ApiOperation("登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public CommonResult login(@Validated @RequestBody User user, BindingResult result){

        CommonResult commonResult;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("username", username);
        queryWrapper.eq("username", user.getUsername())
                .eq("password", user.getPassword());

        user = userService.getOne(queryWrapper);
        if (user != null) {
            commonResult = CommonResult.success(user);
            LOGGER.debug("login success:{}", user);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("login failed:{}", user);
        }
        return commonResult;
    }

    @ApiOperation("创建用户")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CommonResult create(@Validated @RequestBody User user, BindingResult result){

        CommonResult commonResult;
        boolean isok = userService.save(user);
        if (isok) {
            commonResult = CommonResult.success(user);
            LOGGER.debug("createUser success:{}", user);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("createUser failed:{}", user);
        }
        return commonResult;
    }

    @ApiOperation("更新指定id的用户信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public CommonResult update(@Validated @RequestBody User user, BindingResult result){
        CommonResult commonResult;
        boolean isok = userService.updateById(user);
        if (isok) {
            commonResult = CommonResult.success(user);
            LOGGER.debug("updateUser success:{}", user);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("updateUser failed:{}", user);
        }
        return commonResult;
    }

    @ApiOperation("删除指定id的用户信息")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public CommonResult delete(@PathVariable Long id){
        CommonResult commonResult;
        boolean result = userService.removeById(id);
        if (result) {
            commonResult = CommonResult.success(null);
            LOGGER.debug("deleteUser success:id={}", id);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("deleteUser failed:id={}", id);
        }
        return commonResult;
    }


    @ApiOperation("根据名字获取用户信息")
    @RequestMapping(value = "/simpleList", method = RequestMethod.GET)
    public CommonResult getList(@RequestParam(value = "username") String username){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("username", username);
        queryWrapper.like("username", username);
        userService.remove(queryWrapper);

        return CommonResult.success(userService.list(queryWrapper));
    }

    @ApiOperation("分页获取用户信息")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult getList(@RequestParam(value = "pageNum",defaultValue = "1" ) Integer pageNum,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){
        Page<User> page = new Page<>(pageNum, pageSize);
        Page<User> userPage = userService.page(page);
        return CommonResult.success(CommonPage.restPage(userPage));
    }


    // 查询单个用户信息
    // 查询所有用户信息
    // 添加用户
    // 修改用户
    // 删除用户
    // 分页查询用户


}

