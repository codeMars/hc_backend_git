package com.huicewang.aitesting.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huicewang.aitesting.common.CommonPage;
import com.huicewang.aitesting.common.CommonResult;
import com.huicewang.aitesting.model.Project;

import com.huicewang.aitesting.service.ProjectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Api(tags = "ProjectController", description = "项目管理")
@RestController
@RequestMapping("/project")
public class ProjectController {
    // 定义一个日志对象
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;

    // http://localhost:9000/Project/12

    @ApiOperation("获取指定id的项目信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CommonResult<Project> getById(@PathVariable Long id){
        return CommonResult.success(projectService.getById(id));
    }

    @ApiOperation("获取所有项目列表")
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public CommonResult<List<Project>> getList(){
        return CommonResult.success(projectService.list());
    }

    @ApiOperation("创建项目")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CommonResult create(@Validated @RequestBody Project project, BindingResult result){

        CommonResult commonResult;
        boolean isok = projectService.save(project);
        if (isok) {
            commonResult = CommonResult.success(project);
            LOGGER.debug("createProject success:{}", project);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("createProject failed:{}", project);
        }
        return commonResult;
    }

    @ApiOperation("更新指定id的项目信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public CommonResult update(@Validated @RequestBody Project project, BindingResult result){
        CommonResult commonResult;
        boolean isok = projectService.updateById(project);
        if (isok) {
            commonResult = CommonResult.success(project);
            LOGGER.debug("updateProject success:{}", project);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("updateProject failed:{}", project);
        }
        return commonResult;
    }

    @ApiOperation("删除指定id的项目信息")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public CommonResult delete(@PathVariable Long id){
        CommonResult commonResult;
        boolean result = projectService.removeById(id);
        if (result) {
            commonResult = CommonResult.success(null);
            LOGGER.debug("deleteProject success:id={}", id);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("deleteProject failed:id={}", id);
        }
        return commonResult;
    }


    @ApiOperation("根据名字获取项目信息")
    @RequestMapping(value = "/simpleList", method = RequestMethod.GET)
    public CommonResult getList(@RequestParam(value = "name") String name){
        QueryWrapper<Project> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", name);
        return CommonResult.success(projectService.list(queryWrapper));
    }

    @ApiOperation("分页获取项目信息")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult getList(@RequestParam(value = "pageNum",defaultValue = "1" ) Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){
        Page<Project> page = new Page<>(pageNum, pageSize);
        Page<Project> projectPage = projectService.page(page);
        return CommonResult.success(CommonPage.restPage(projectPage));
    }
}

