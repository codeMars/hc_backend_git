package com.huicewang.aitesting.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huicewang.aitesting.common.CommonResult;
import com.huicewang.aitesting.form.ApiForm;
import com.huicewang.aitesting.model.Api;
import com.huicewang.aitesting.model.Apigroup;
import com.huicewang.aitesting.model.User;
import com.huicewang.aitesting.service.ApiService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    // 定义一个日志对象
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private ApiService apiService;


    @ApiOperation("创建接口")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CommonResult create(@Validated @RequestBody Api api){

        CommonResult commonResult;

        boolean isok = apiService.save(api);
        if (isok) {
            commonResult = CommonResult.success(api);
            LOGGER.debug("createProject success:{}", api);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("createProject failed:{}", api);
        }
        return commonResult;
    }


    @ApiOperation("更新指定id的接口")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult update(@Validated @RequestBody Api api, BindingResult result){
        CommonResult commonResult;
        boolean isok = apiService.updateById(api);
        if (isok) {
            commonResult = CommonResult.success(api);
            LOGGER.debug("updateProject success:{}", api);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("updateProject failed:{}", api);
        }
        return commonResult;
    }

    @ApiOperation("删除指定id的接口信息")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public CommonResult delete(@PathVariable Long id){
        CommonResult commonResult;
        boolean result = apiService.removeById(id);
        if (result) {
            commonResult = CommonResult.success(null);
            LOGGER.debug("deleteProject success:id={}", id);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("deleteProject failed:id={}", id);
        }
        return commonResult;
    }

    @ApiOperation("根据项目id 查询接口")
    @RequestMapping(value = "/lists", method = RequestMethod.GET)
    public CommonResult<Apigroup> getById(@RequestParam(value = "projectId",required =true) Integer projectId,
                                          @RequestParam(value = "apiGroupId",required =false) Integer apiGroupId,
                                          @RequestParam(value = "id",required =false) Integer id,
                                          @RequestParam(value = "pageSize") Integer pageSize,
                                          @RequestParam(value = "pageIndex") Integer pageIndex
                                         ){


        List<Api> list =  apiService.getInterface(projectId,apiGroupId,id,pageIndex,pageSize);

        Integer listCount = apiService.getInterfaceCount(projectId,apiGroupId,id);

        Map<String, Object> result = new HashMap<String, Object>() {{
            put("interfaceList", list);
            put("interfaceCount", listCount);
        }};

        return CommonResult.success(result);
    }


    @ApiOperation("根据项目id 查询接口")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<Apigroup> getByIds(@RequestParam(value = "projectId",required =false) Integer projectId,
                                          @RequestParam(value = "apiGroupId",required =false) Integer apiGroupId,
                                          @RequestParam(value = "id",required =false) Integer id,
                                          @RequestParam(value = "pageSize",required =false) Integer pageSize,
                                          @RequestParam(value = "pageIndex",required =false) Integer pageIndex
    ){

        QueryWrapper<Api> queryWrapper = new QueryWrapper<>();

        if(projectId != null){
            queryWrapper.like("projectId",projectId);
        }
        if(apiGroupId != null){
            queryWrapper.like("apiGroupId",apiGroupId);
        }
        if(id !=null){
            queryWrapper.eq("id",id);
        }
        //构建条件
        Page<Api> page = new Page<>(pageIndex,pageSize);
        return CommonResult.success(apiService.page(page,queryWrapper));
    }
}

