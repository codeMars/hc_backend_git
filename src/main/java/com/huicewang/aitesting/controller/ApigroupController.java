package com.huicewang.aitesting.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huicewang.aitesting.bean.ApiBean;
import com.huicewang.aitesting.bean.ApiGroupTempBean;
import com.huicewang.aitesting.common.CommonResult;
import com.huicewang.aitesting.model.Api;
import com.huicewang.aitesting.model.Apigroup;
import com.huicewang.aitesting.model.Project;
import com.huicewang.aitesting.service.ApiService;
import com.huicewang.aitesting.service.ApigroupService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@RestController
@RequestMapping("/apigroup")
public class ApigroupController {

    // 定义一个日志对象
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ApigroupService apigroupService;
    @Autowired
    private ApiService apiService;



    @ApiOperation("创建接口分组")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CommonResult create(@Validated @RequestBody Apigroup apigroup, BindingResult result){

        CommonResult commonResult;
        boolean isok = apigroupService.save(apigroup);
        if (isok) {
            commonResult = CommonResult.success(apigroup);
            LOGGER.debug("createProject success:{}", apigroup);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("createProject failed:{}", apigroup);
        }
        return commonResult;
    }

    @ApiOperation("根据项目id 查询分组")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<Apigroup> getById(@RequestParam Long projectId){

        Map<String, Object> columnMap = new HashMap<String, Object>();
        columnMap.put("projectId",projectId);
        List<Apigroup>  list = apigroupService.listByMap(columnMap);

        List<ApiGroupTempBean> listTmp = new ArrayList<>();
        for (int i=0;i<list.size();i++){
            ApiGroupTempBean ApiGroupTempBean = new ApiGroupTempBean();
            ApiGroupTempBean.setId(list.get(i).getId());
            ApiGroupTempBean.setLabel(list.get(i).getName());
            ApiGroupTempBean.setDesc(list.get(i).getDescription());

            //查询该项目下接口
            QueryWrapper<Api> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("projectId",projectId).eq("apiGroupId",list.get(i).getId());

            List<Api> apiList = apiService.list(queryWrapper);
            List<ApiBean> ApiBeanList = new ArrayList<>();
            for (int j=0;j<apiList.size();j++){
                ApiBean bean = new ApiBean();
                bean.setLabel(apiList.get(j).getName());
                bean.setId(apiList.get(j).getId());
                bean.setDescription(apiList.get(j).getDescription());
                ApiBeanList.add(bean);
            }

            ApiGroupTempBean.setChildren(ApiBeanList);

            listTmp.add(ApiGroupTempBean);
        }
        return CommonResult.success(listTmp);
    }

    @ApiOperation("更新指定id的分组")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult update(@Validated @RequestBody Apigroup apigroup, BindingResult result){
        CommonResult commonResult;
        boolean isok = apigroupService.updateById(apigroup);
        if (isok) {
            commonResult = CommonResult.success(apigroup);
            LOGGER.debug("updateProject success:{}", apigroup);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("updateProject failed:{}", apigroup);
        }
        return commonResult;
    }

    @ApiOperation("删除指定id的分组信息")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public CommonResult delete(@PathVariable Long id){
        CommonResult commonResult;
        boolean result = apigroupService.removeById(id);
        if (result) {
            commonResult = CommonResult.success(null);
            LOGGER.debug("deleteProject success:id={}", id);
        }else {
            commonResult = CommonResult.failed();
            LOGGER.debug("deleteProject failed:id={}", id);
        }
        return commonResult;
    }


    @ApiOperation("获取所有接口分组列表")
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public CommonResult<List<Project>> getList(){

        return CommonResult.success(apigroupService.list());
    }




}

