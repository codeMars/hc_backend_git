package com.huicewang.aitesting;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

//@MapperScan("com.huicewang.aitesting.dao")
@SpringBootApplication
public class AitestingApplication {
	public static void main(String[] args) {
		SpringApplication.run(AitestingApplication.class, args);
	}

}
