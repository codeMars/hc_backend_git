package com.huicewang.aitesting.bean;

import com.huicewang.aitesting.model.Api;

import java.util.List;

/**
 * @program: aitesting
 * @author: caosenquan
 * @create: 2021-08-19 15:24
 **/

public class ApiGroupTempBean {

    private Integer id;

    private Integer orderCode;
    private String label;
    private String desc;
    private List<ApiBean> children;


    @Override
    public String toString() {
        return "ApiGroupTempBean{" +
                "id=" + id +
                ", orderCode=" + orderCode +
                ", label='" + label + '\'' +
                ", desc='" + desc + '\'' +
                ", children=" + children +
                '}';
    }

    public List<ApiBean> getChildren() {
        return children;
    }

    public void setChildren(List<ApiBean> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(Integer orderCode) {
        this.orderCode = orderCode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
