package com.huicewang.aitesting.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TestCaseExecutorTask {
    private Logger LOGGER = LoggerFactory.getLogger(TestCaseExecutorTask.class);

    @Scheduled(cron = "0 0/1 * ? * ?")  //定时执行计划
    private void executeCase(){
        LOGGER.info("被执行到一次");
    }

}
