package com.huicewang.aitesting.component;

import com.huicewang.aitesting.controller.UserController;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

@Aspect
@Component
@Order(1)
public class WebLogAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebLogAspect.class);

    // com.huicewang.aitesting.controller包中所有类的public方法都应用切面里的通知
    @Pointcut("execution(public * com.huicewang.aitesting.controller.*.*(..))")
    public void webLog(){
    }
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (attributes == null){
            return;
        }
        //获取request对象
        HttpServletRequest request = attributes.getRequest();
        String ip = request.getRemoteAddr();
        String time = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss").format(new Date());
        String target = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        LOGGER.info(String.format("用户[%s]，在[%s]，访问了[%s].",ip, time, target));
    }
//    @Around("webLog()")
//    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
//
//        // 用户 [ip地址] 在某个时间访问了什么服务
//        long startTime = System.currentTimeMillis();
//        Object result = joinPoint.proceed();
//        long endTime = System.currentTimeMillis();
//        long duration = endTime - startTime;
//        LOGGER.info("耗时：" + duration);
//        return result;
//    }


}
