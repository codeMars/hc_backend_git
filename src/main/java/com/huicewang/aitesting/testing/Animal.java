package com.huicewang.aitesting.testing;

public class Animal {
    public void eat(){
        System.out.println("我正在吃肉");
    }
    public void sleep(){
        System.out.println("我正在睡觉");
    }
    public void shout(){
        System.out.println("我在叫");
    }

    private Animal(){

    }
    public static Animal getInstance(){
        return new Animal();
    }
    public static void main(String[] args) {
        Animal animal = new Animal();
    }
}
