package com.huicewang.aitesting.common;

public interface IErrorCode {
    long getCode();
    String getMessage();
}
