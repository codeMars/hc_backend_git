package com.huicewang.aitesting.common;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CommonPage<T> {
    private Integer pageNum;
    private Integer pageSize;
    private Integer totalPage;
    private Long total;
    private List<T> list;

    public static <T> CommonPage<T> restPage(Page<T> pageResult){
        CommonPage<T> page = new CommonPage<T>();
        page.setPageNum(Convert.toInt(pageResult.getCurrent()));
        page.setPageSize(Convert.toInt(pageResult.getSize()));
        page.setTotal(pageResult.getTotal());
        page.setTotalPage(Convert.toInt(pageResult.getPages()));//Convert.toInt(pageResult.getTotal()/pageResult.getSize() + 1));
        page.setList(pageResult.getRecords());
        return page;
    }

}
