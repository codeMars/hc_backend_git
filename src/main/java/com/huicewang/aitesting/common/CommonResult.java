package com.huicewang.aitesting.common;

import lombok.Getter;

@Getter
public class CommonResult<T> {

    private long code;
    private String message;
    private T data;
    public CommonResult(long code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 封装默认的成功信息
     * @param data 获取的数据信息
     * @param <T> 数据类型
     * @return
     */
    public static <T> CommonResult success(T data){
        return new CommonResult<>(CommonCode.SUCCESS.getCode(), CommonCode.SUCCESS.getMessage(), data);
    }

    /**
     * 自定义状态码及返回信息
     * @param data
     * @param code
     * @param message
     * @param <T>
     * @return
     */
    public static <T> CommonResult success(T data, long code, String message){
        return new CommonResult<>(code, message, data);
    }

    /**
     * 自定义返回信息
     * @param data
     * @param message
     * @param <T>
     * @return
     */
    public static <T> CommonResult success(T data, String message){
        return new CommonResult<>(CommonCode.SUCCESS.getCode(), message, data);
    }

    public static<T> CommonResult failed(String message){
        return new CommonResult<>(CommonCode.FAILURE.getCode(), CommonCode.FAILURE.getMessage(),null);
    }

    public static<T> CommonResult failed(IErrorCode resultCode){
        return new CommonResult<>(resultCode.getCode(), resultCode.getMessage(),null);
    }

    public static<T> CommonResult failed(IErrorCode resultCode, T data){
        return new CommonResult<>(resultCode.getCode(), resultCode.getMessage(),data);
    }

    public static<T> CommonResult failed(){
        return failed(CommonCode.FAILURE);
    }

    public static<T> CommonResult validateFailed(){
        return failed(CommonCode.VALIDATE_FAILED);
    }

    public static<T> CommonResult validateFailed(String message){
        return new CommonResult<>(CommonCode.VALIDATE_FAILED.getCode(), message,null);
    }

    public static<T> CommonResult unauthorized(T data){
        return failed(CommonCode.UNAUTHORIZED, data);
    }

    public static<T> CommonResult forbidden(T data){
        return failed(CommonCode.FORBIDDEN, data);
    }

//
//    public static void main(String[] args) {
//        CommonResult.failed(CommonCode.SUCCESS);
//        CommonResult.failed(BusinessCode.ORDER_FAILED);
//    }

}
