package com.huicewang.aitesting.common;

public enum BusinessCode implements IErrorCode{
    ORDER_FAILED(2000,"数据插入失败")
    ;

    long code;
    String message;
    BusinessCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
