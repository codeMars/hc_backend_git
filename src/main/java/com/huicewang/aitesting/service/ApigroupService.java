package com.huicewang.aitesting.service;

import com.huicewang.aitesting.model.Apigroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
public interface ApigroupService extends IService<Apigroup> {

}
