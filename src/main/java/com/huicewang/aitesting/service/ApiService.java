package com.huicewang.aitesting.service;

import com.huicewang.aitesting.model.Api;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huice
 * @since 2021-08-21
 */
public interface ApiService extends IService<Api> {



    //查询接口
    List<Api> getInterface(Integer projectId,Integer apiGroupId,Integer id,int pageIndex,int pageSize );


    //查询接口总数
    Integer getInterfaceCount(Integer projectId,Integer apiGroupId,Integer id);







}
