package com.huicewang.aitesting.service;

import com.huicewang.aitesting.model.Case;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
public interface CaseService extends IService<Case> {

}
