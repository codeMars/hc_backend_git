package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Alarm;
import com.huicewang.aitesting.mapper.AlarmMapper;
import com.huicewang.aitesting.service.AlarmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Service
public class AlarmServiceImpl extends ServiceImpl<AlarmMapper, Alarm> implements AlarmService {

}
