package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Case;
import com.huicewang.aitesting.mapper.CaseMapper;
import com.huicewang.aitesting.service.CaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Service
public class CaseServiceImpl extends ServiceImpl<CaseMapper, Case> implements CaseService {

}
