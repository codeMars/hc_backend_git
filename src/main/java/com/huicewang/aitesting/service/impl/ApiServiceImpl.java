package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Api;
import com.huicewang.aitesting.mapper.ApiMapper;
import com.huicewang.aitesting.service.ApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-08-21
 */
@Service
public class ApiServiceImpl extends ServiceImpl<ApiMapper, Api> implements ApiService {


    @Autowired
    private ApiMapper apiMapper;



    @Override
    public List<Api> getInterface(Integer projectId,Integer apiGroupId,Integer id,int pageIndex,int pageSize) {

        Integer startSize = (pageIndex - 1) * pageSize;
        return apiMapper.getInterface(projectId, apiGroupId, id, startSize, pageSize );
    }

    @Override
    public Integer getInterfaceCount(Integer projectId,Integer apiGroupId,Integer id) {

        return apiMapper.getInterfaceCount( projectId, apiGroupId, id);

    }
}
