package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.controller.UserController;
import com.huicewang.aitesting.model.User;
import com.huicewang.aitesting.mapper.UserMapper;
import com.huicewang.aitesting.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    public void get(Integer id) {
        check(id);
        service(id);
        redis(id);
        mysql(id);
    }

    public void service(Integer id){
        long count = 0;
        for(int i = 0; i < 100; i++){
            count++;
        }
        LOGGER.info("service end {}", count);
    }

    public void check(Integer id) {
        if (id == null || id < 0) {
            LOGGER.error("id非法");
           // throw new Exception("id非法");
        }
    }

    public void mysql(Integer id){
        long count = 0;
        for(int i = 0; i < 100000000; i++){
            count++;
        }
        LOGGER.info("mysql end {}", count);
    }
    public void redis(Integer id){
        int count = 0;
        for (int i = 0; i < 10000; i++){
            count++;
        }
        LOGGER.info("redis end {}", count);
    }
}
