package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Parameters;
import com.huicewang.aitesting.mapper.ParametersMapper;
import com.huicewang.aitesting.service.ParametersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Service
public class ParametersServiceImpl extends ServiceImpl<ParametersMapper, Parameters> implements ParametersService {

}
