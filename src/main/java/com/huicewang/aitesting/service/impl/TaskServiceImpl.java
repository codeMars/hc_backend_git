package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Task;
import com.huicewang.aitesting.mapper.TaskMapper;
import com.huicewang.aitesting.service.TaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {

}
