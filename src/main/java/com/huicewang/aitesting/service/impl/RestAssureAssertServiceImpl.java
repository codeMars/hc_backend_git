package com.huicewang.aitesting.service.impl;

import com.alibaba.fastjson.JSON;
import com.huicewang.aitesting.service.RestAssureAssertService;
import io.restassured.response.Response;
import org.springframework.stereotype.Service;

import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@Service
public class RestAssureAssertServiceImpl implements RestAssureAssertService {
    public void assertRestAssure(Response response, String expected) {
        Map<String, Object> expectedMap = JSON.parseObject(expected);
        Map<String, Object> checkedValue = null;
        for (String key : expectedMap.keySet()) {
            //key = "equalTo"
            if (key.equals("equalTo")) {
                checkedValue = JSON.parseObject(expectedMap.get(key).toString());//{"code":200,"message":"获取验证码成功"}
                for (String subkey : checkedValue.keySet()) {
                    response.then().body(subkey, equalTo(checkedValue.get(subkey).toString()));
                }
            } else if (key.equals("containsString")) {
                checkedValue = JSON.parseObject(expectedMap.get(key).toString());//{"code":200,"message":"获取验证码成功"}
                for (String subkey : checkedValue.keySet()) {
                    response.then().body(subkey, containsString(checkedValue.get(subkey).toString()));
                }
            }
        }
    }
}