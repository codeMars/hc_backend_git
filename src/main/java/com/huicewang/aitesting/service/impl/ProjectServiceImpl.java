package com.huicewang.aitesting.service.impl;

import com.huicewang.aitesting.model.Project;
import com.huicewang.aitesting.mapper.ProjectMapper;
import com.huicewang.aitesting.service.ProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements ProjectService {

}
