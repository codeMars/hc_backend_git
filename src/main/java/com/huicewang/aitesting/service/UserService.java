package com.huicewang.aitesting.service;

import com.huicewang.aitesting.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huice
 * @since 2021-07-14
 */
public interface UserService extends IService<User> {

}
