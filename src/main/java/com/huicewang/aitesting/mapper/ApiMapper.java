package com.huicewang.aitesting.mapper;

import com.huicewang.aitesting.model.Api;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huice
 * @since 2021-08-21
 */
public interface ApiMapper extends BaseMapper<Api> {


    //查询接口
    List<Api> getInterface(Integer projectId,Integer apiGroupId,Integer id,int startSize,int pageSize );


    //查询接口总数
    Integer getInterfaceCount(Integer projectId,Integer apiGroupId,Integer id);



}
