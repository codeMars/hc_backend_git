package com.huicewang.aitesting.mapper;

import com.huicewang.aitesting.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huice
 * @since 2021-07-14
 */
//@Mapper
public interface UserMapper extends BaseMapper<User> {

}
