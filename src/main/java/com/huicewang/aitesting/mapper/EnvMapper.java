package com.huicewang.aitesting.mapper;

import com.huicewang.aitesting.model.Env;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huice
 * @since 2021-07-30
 */
public interface EnvMapper extends BaseMapper<Env> {

}
